import pytest
import unittest

import ipv6


def test_ipv6_expand():                
    retval = ipv6.ipv6_expand("2001:4860:4860::8888")
    assert retval == "2001:4860:4860:0000:0000:0000:0000:8888"
   
def test_ipv6_compress():                
    retval = ipv6.ipv6_compress("2001:4860:4860:0000:0000:0000:0000:8888")
    assert retval == "2001:4860:4860::8888"
   
def test_bad_chars_ipv6():
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress("xxxx:4860:4860::hgfg")
        
def test_bad_octet_ipv6():
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress("20012:4860:4860::2001")
        
def test_bad_length_ipv6():
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress("2001:0db8:0000:0000:0000:0000:1428:57ab:1428")
        
def test_ipv6_compress2():   
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress("2001:db8")

def test_ipv6_compress3():   
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress(":AAAA:")
        
def test_ipv6_compress4():   
    with  pytest.raises(ValueError) as e_info:
        retval = ipv6.ipv6_compress(":AAAA::")

def test_ipv6_compress5():   
    retval = ipv6.ipv6_compress("2001:db8:0:B::1a")
    assert retval == "2001:db8:0:B::1a"

