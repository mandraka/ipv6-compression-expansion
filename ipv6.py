import os, sys
import json
import getopt

def check_ipv6(ipv6):
    if 1 in [c in ipv6 for c in 'ghijklmnoprstquxyz!@#$%^&*()[]{};\'\\/,.<>"']:
        raise ValueError("Forbidden characters in IPv6")
    octets = ipv6.split(':')
    for octet in octets:
        if len(octet) > 4:
            raise ValueError("IPv6 is badly constructed")
    if len(octets) > 8:
        raise ValueError("IPv6 is too long")

def ipv6_compress(ipv6):
    check_ipv6(ipv6)
    octets = ipv6.split(':')
    compactedOctets = []
    for octet in octets:
        while len(octet) > 1 and octet[0] == '0':
            octet = octet[-len(octet)+1:]
        
        compactedOctets.append(octet)
    
    mergedOctets = ""
    for compactedOctet in compactedOctets:
        mergedOctets+=compactedOctet+':'
        
    startCompressing = mergedOctets.find(":0")
    endCompressing = mergedOctets[::-1].find(":0:")
    if startCompressing != endCompressing:
        mergedOctets = mergedOctets[0:startCompressing]+"::"+mergedOctets[len(mergedOctets)-endCompressing:]
        
    compressedIpv6 = mergedOctets[:-1]
    return(compressedIpv6)

def ipv6_expand(ipv6):
    check_ipv6(ipv6)
    octets = ipv6.split(':')
    octets_to_add = 8 - len(octets)
    
    fullOctets = []
    timesExpanded = 0
    for octet in octets:
        fullOctets.append(octet)
        if len(octet) == 0:
            timesExpanded += 1
            if timesExpanded == 1:
                for i in range(0,octets_to_add):
                    fullOctets.append('')
                    
    mergedOctets=''
    for fullOctet in fullOctets:
        while len(fullOctet) != 4:
            fullOctet = '0' + fullOctet
        mergedOctets += fullOctet+':'

    expandedIpv6 = mergedOctets.strip(':')
    return(expandedIpv6)

def usage():
    print ("ipv6 -p path_to_json_file -c expand|compress")

if __name__ == '__main__':
    argv = sys.argv[1:]
    if len(argv) == 0:
      print("No arguments passed, write -h or --help")
      sys.exit()
    opts, args = getopt.getopt(argv, 'hp:c:', 'help')
    for o, a in opts:
        if o == '-p':
            json_path = a
        elif o == '-c':
            if a not in ('expand', 'compress'):
                raise ValueError("Wrong command")
            command = a
        elif o in ('-h', '--help'):
            usage()
            sys.exit()
        else:
            assert False, 'unhandled option'

    try:
        command
    except NameError:
        raise Exception("Command has not been passed")
        
    try:
        json_path
    except NameError:
        raise Exception("Path to IPv6s has not been passed")

    if not os.path.isfile(json_path):
        raise IOError("No such file " + json_path)

    
    #read json
    file = open(json_path, 'r')
    json_data = json.load(file)
    for interface in json_data['interfaces']:
        print(interface['address'])
        if command == 'expand':
            interface['address'] = ipv6_expand(interface['address'])
        elif command == 'compress':
            interface['address'] = ipv6_compress(interface['address'])
        print(interface['address'])
        print()
    file.close()
    
    #save json
    file = open(json_path, 'w')
    json.dump(json_data, file)
    file.close()
    